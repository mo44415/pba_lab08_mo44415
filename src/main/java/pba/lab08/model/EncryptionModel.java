package pba.lab08.model;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class EncryptionModel {
    @NotBlank
    private String data;
    @NotBlank
    private String encryptionKey;
}
