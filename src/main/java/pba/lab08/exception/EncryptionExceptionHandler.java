package pba.lab08.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class EncryptionExceptionHandler {
    @ExceptionHandler({EncryptionException.class})
    public ResponseEntity<String> handleEncryptionException(EncryptionException exception) {
        return ResponseEntity.badRequest().body(exception.getMessage());
    }
}
