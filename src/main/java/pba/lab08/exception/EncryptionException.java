package pba.lab08.exception;

import org.springframework.http.HttpStatus;

public class EncryptionException extends RuntimeException {
    public EncryptionException(Throwable cause) {
        super(HttpStatus.BAD_REQUEST.toString(), cause);
    }

    public EncryptionException(String message, Throwable cause) {
        super(message, cause);
    }
}
