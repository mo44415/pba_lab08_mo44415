package pba.lab08.constants;

public class EncryptionConstants {
    public static final String RSA_ALGORITHM = "RSA";
    public static final String AES_ALGORITHM = "AES";

    public static final String SECRET_KEY_ALGORITHM = "PBKDF2WithHmacSHA256";
    public static final int RSA_KEY_SIZE = 2048;
    public static final int AES_KEY_SIZE = 256;
}
