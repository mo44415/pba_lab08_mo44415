package pba.lab08.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pba.lab08.model.EncryptionModel;
import pba.lab08.service.EncryptionService;

import java.util.Map;

@RestController
@RequestMapping(path = "/api")
@RequiredArgsConstructor
public class EncryptionController {
    private final EncryptionService encryptionService;

    @PostMapping("/encrypt")
    public ResponseEntity<EncryptionModel> encrypt(@RequestBody Map<String, Object> request) {
        return ResponseEntity.ok().body(encryptionService.encrypt(request));
    }

    @PostMapping("/decrypt")
    public ResponseEntity<Map<String, Object>> decrypt(@RequestBody EncryptionModel request) throws JsonProcessingException {
        return ResponseEntity.ok().body(encryptionService.decrypt(request));
    }
}
