package pba.lab08.mapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import pba.lab08.exception.EncryptionException;

import java.util.Map;

@Component
public class EncryptionMapper {

    @Autowired
    ObjectMapper objectMapper;

    public String mapJsonToString(Map<String, Object> request) {
        try {
            return objectMapper.writeValueAsString(request);
        } catch (JsonProcessingException e) {
            throw new EncryptionException("Invalid input json", e);
        }
    }

    public Map<String, Object> mapStringToJson(String json) {
        try {
            return objectMapper.readValue(json, new TypeReference<Map<String, Object>>() {
            });
        } catch (JsonProcessingException e) {
            throw new EncryptionException("Invalid input json", e);
        }
    }
}
