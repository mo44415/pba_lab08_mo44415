package pba.lab08.service;

import org.springframework.stereotype.Service;
import pba.lab08.constants.EncryptionConstants;
import pba.lab08.exception.EncryptionException;
import pba.lab08.mapper.EncryptionMapper;
import pba.lab08.model.EncryptionModel;
import pba.lab08.utils.EncryptionUtils;

import javax.crypto.SecretKey;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Map;

@Service

public class EncryptionService {

    private final EncryptionMapper mapper;
    private final KeyPair keyPair;

    public EncryptionService(EncryptionMapper mapper) throws NoSuchAlgorithmException {
        this.mapper = mapper;
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance(EncryptionConstants.RSA_ALGORITHM);
        keyPairGenerator.initialize(EncryptionConstants.RSA_KEY_SIZE);
        this.keyPair = keyPairGenerator.generateKeyPair();
    }

    public EncryptionModel encrypt(Map<String, Object> request) {
        String message = mapper.mapJsonToString(request);
        try {
            String encryptedRSA = EncryptionUtils.encryptRSA(message, keyPair.getPublic());
            SecretKey secretKey = EncryptionUtils.generateRandomSecret();
            String encryptedSecret = EncryptionUtils.encryptRSA(Base64.getEncoder().encodeToString(secretKey.getEncoded()), keyPair.getPublic());
            String encryptedAES = EncryptionUtils.encryptAES(encryptedRSA, secretKey);
            return new EncryptionModel(encryptedAES, encryptedSecret);
        } catch (Exception e) {
            throw new EncryptionException(e);
        }
    }

    public Map<String, Object> decrypt(EncryptionModel request) {
        try {
            String decryptedSecret = EncryptionUtils.decryptRSA(request.getEncryptionKey(), keyPair.getPrivate());
            String decryptedAES = EncryptionUtils.decryptAES(request.getData(), decryptedSecret);
            String decryptedRSA = EncryptionUtils.decryptRSA(decryptedAES, keyPair.getPrivate());
            return mapper.mapStringToJson(decryptedRSA);
        } catch (Exception e) {
            throw new EncryptionException(e);
        }
    }
}
